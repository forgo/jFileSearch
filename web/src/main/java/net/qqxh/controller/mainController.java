package net.qqxh.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import net.qqxh.persistent.JfSysUserData;
import net.qqxh.persistent.JfUserSimple;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Controller
public class mainController extends BaseController {
    @Autowired
    ShiroFilterFactoryBean shiroFilterFactoryBean;
    @RequestMapping("/")
    public String index(ModelMap map) {
        JfUserSimple jfUserSimple = getLoginUser();
        if(jfUserSimple==null){
            jfUserSimple=new JfUserSimple();
        }
        map.addAttribute("permissions", "");
        map.addAttribute("jfShiroUser", jfUserSimple);
        if (SecurityUtils.getSubject().isAuthenticated()||SecurityUtils.getSubject().isRemembered()) {
            return "index";
        }

        return "login";
    }

    @RequestMapping("/tologin")
    public String tologin(ModelMap map, @RequestParam(name = "username", required = false) String username, @RequestParam(name = "password", required = false) String password) {
            return "login";
    }
    @RequestMapping("/login")
    public String login(ModelMap map, @RequestParam(name = "username", required = false) String username, @RequestParam(name = "password", required = false) String password) {
       if(StringUtils.isEmpty(username)){
           return "login";
       }
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
        try {
            usernamePasswordToken.setRememberMe(true);
            subject.login(usernamePasswordToken);
        } catch (AuthenticationException e) {
          e.printStackTrace();
            return "login";
        }
        return "redirect:/";
    }
    @ResponseBody
    @RequestMapping("/changeTheme")
    public Object changeTheme(ModelMap map, @RequestParam(name = "theme", required = false) String theme) {
        JfUserSimple jfUserSimple = getLoginUser();
        if(jfUserSimple==null){
            return null;
        }
        jfUserSimple.setTheme(theme);
        JfUserSimple userdb=JfSysUserData.getUserByUserId(jfUserSimple.getUserid());
        userdb.setTheme(theme);
        return responseJsonFactory.getSuccessJson("修改主题成功","");
    }
    /**
     * 更新系统权限缓存数据方法。后台配置改变之后需要调用该方法进行刷新缓存信息
     */
    @RequestMapping("/updatePermission")
    @ResponseBody
    public Object updatePermission() {

        synchronized (shiroFilterFactoryBean) {

            AbstractShiroFilter shiroFilter = null;
            try {
                shiroFilter = (AbstractShiroFilter) shiroFilterFactoryBean
                        .getObject();
            } catch (Exception e) {
                throw new RuntimeException(
                        "get ShiroFilter from shiroFilterFactoryBean error!");
            }

            PathMatchingFilterChainResolver filterChainResolver = (PathMatchingFilterChainResolver) shiroFilter
                    .getFilterChainResolver();
            DefaultFilterChainManager manager = (DefaultFilterChainManager) filterChainResolver
                    .getFilterChainManager();

            // 清空老的权限控制
            manager.getFilterChains().clear();
            shiroFilterFactoryBean.getFilterChainDefinitionMap().clear();
            shiroFilterFactoryBean.setFilterChainDefinitionMap(getPermissions());
            // 重新构建生成
            Map<String, String> chains = shiroFilterFactoryBean.getFilterChainDefinitionMap();
            for (Map.Entry<String, String> entry : chains.entrySet()) {
                String url = entry.getKey();
                String chainDefinition = entry.getValue().trim()
                        .replace(" ", "");
                manager.createChain(url, chainDefinition);
            }

        }
        return responseJsonFactory.getSuccessJson("刷新权限缓存成功","");
    }
    public Map getPermissions() {
        Map<String, String> permissions = new HashMap<>();
        ;
        try {
            ClassPathResource resource = new ClassPathResource("realmDB.json");
            InputStream inputStream = resource.getInputStream();
            String realmDBStr = IOUtils.toString(inputStream, "UTF-8");
            JSONObject jsonObject = JSONObject.parseObject(realmDBStr);
            JSONArray permissionsData = (JSONArray) jsonObject.get("permissions");
            for (Object o : permissionsData) {
                JSONObject j = (JSONObject) o;
                permissions.put(j.getString("url"), j.getString("permission"));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return permissions;
    }

}
