package net.qqxh.controller.apiUtil;

public class ApiJsonFactory {

    public BaseJson getSuccessJson(String detaile, Object data) {
        BaseJson baseJson = new BaseJson();
        baseJson.setStatus("200");
        baseJson.setMsg("成功");
        baseJson.setDetail(detaile);
        baseJson.setData(data);
        return baseJson;
    }

    public BaseJson getErrorJson(String detaile, Object data) {
        BaseJson baseJson = new BaseJson();
        baseJson.setStatus("500");
        baseJson.setMsg("失败");
        baseJson.setDetail(detaile);
        baseJson.setData(data);
        return baseJson;
    }

}
