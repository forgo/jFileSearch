package net.qqxh.controller;

public class FileViewRouter {
    private static String FDF_VIEW = "viewer/pdf";
    private static String TXT_VIEW = "viewer/txt";
    public static String router(String fix) {

        switch (fix) {
            case "pdf":
                return FDF_VIEW;
            case "txt":
                return TXT_VIEW;
        }
        return "";
    }
}
