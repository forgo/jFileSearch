var options = {
    multiple: true, /*能否同时展开多个*/
    defaut_target: "#_content",
    base_url:"/aitaoker/src/problemCenter.html",/*项目根目录，初次加载后路由会以这个为跟路径*/
    menuList: [
        {
            name: "帮助中心",
            id: "1",
            pid: "0",
            isParent: true,
            url: "content/helpCenter/commonProblem.html",
            router: "",
            fa_icon: "fa fa-home",open:true,
            children: [
                {
                    name: "常见问题",
                    id: "11",
                    pid: "1",
                    isParent: false,
                    url: "content/helpCenter/commonProblem.html",
                    target: "#_content",
                    router: "cjwt"
                },
                {
                    name: "新手指南",
                    id: "12",
                    pid: "1",
                    isParent: false,
                    url: "content/helpCenter/commonProblem.html",
                    target: "#_content",
                    router: "xszn"
                }
            ]
        },
        {
            name: "规则说明",
            id: "1",
            pid: "0",
            isParent: true,
            url: "content/helpCenter/commonProblem.html",
            router: "gzsm",
            open: false,
            fa_icon: "fa fa-list-alt",open:true,
            children: [
                {
                    name: "充值扣费说明",
                    id: "11",
                    pid: "1",
                    isParent: false,
                    url: "content/helpCenter/commonProblem.html",
                    router: "czkgsm",
                    target: "#_content"
                },
                {
                    name: "信用分说明",
                    id: "12",
                    pid: "1",
                    isParent: false,
                    url: "content/helpCenter/commonProblem.html",
                    router: "xyfsm",
                    target: "#_content"
                },{
                    name: "点券金额说明",
                    id: "12",
                    pid: "1",
                    isParent: false,
                    url: "content/helpCenter/commonProblem.html",
                    router: "dqjesm",
                    target: "#_content"
                }
            ]
        },
        {
            name: "其他问题",
            id: "12",
            pid: "1",
            isParent: true,
            target: "#_content",
            url: "content/helpCenter/commonProblem.html",
            router: "qtwt",
            fa_icon: "fa fa-vimeo-square",open:true,
            children: [
                {
                    name: "其他问题",
                    id: "11",
                    pid: "1",
                    isParent: false,
                    url: "content/helpCenter/commonProblem.html",
                    target: "#_content",
                    router: "qtwt"
                },
                {
                    name: "其他问题",
                    id: "12",
                    pid: "1",
                    isParent: false,
                    url: "content/helpCenter/commonProblem.html",
                    target: "#_content",
                    router: "qtwt2"
                },
                {
                    name: "其他问题",
                    id: "12",
                    pid: "1",
                    isParent: false,
                    url: "content/helpCenter/commonProblem.html",
                    target: "#_content",
                    router: "qtwt3"
                }
            ]
        }

    ]
};
$(function () {
    $(".nav").navigationMenu(options);



});


